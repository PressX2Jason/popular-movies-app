package com.pressx2jason.popularmoviesapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class PosterDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poster_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction().add(
                    R.id.container_poster_detail,
                    new PosterDetailFragment()
            ).commit();
        }
    }
}
