package com.pressx2jason.popularmoviesapp;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Jason on 10/17/2015.
 */
public class PostersFragment extends Fragment {

    private final String LOG_TAG = PostersFragment.class.getSimpleName();
    private MoviePosterAdapter moviePosterAdapter;
    private Spinner spinner;

    public PostersFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        fetchPosters();
        super.onStart();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.menu_main, menu);
        spinner = (Spinner) menu.findItem(R.id.spinner_sort).getActionView();

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.sort_selection, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                fetchPosters();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        moviePosterAdapter = new MoviePosterAdapter(getActivity(), new ArrayList<Movie>());

        //inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_posters, container, false);
        GridView gridview = (GridView) rootView.findViewById(R.id.gridview_posters);
        gridview.setAdapter(moviePosterAdapter);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                Intent intent = new Intent(rootView.getContext(), PosterDetailActivity.class);
                intent.putExtra(Movie.class.getSimpleName(), moviePosterAdapter.getItem(position));
                startActivity(intent);
            }
        });
        return rootView;
    }

    private void fetchPosters() {
        String apiKey = getResources().getString(R.string.api_key);
        FetchMovieTask fetchMovieTask = new FetchMovieTask();
        if (spinner != null) {
            fetchMovieTask.execute(apiKey, spinner.getSelectedItem().toString());
        }else{
            fetchMovieTask.execute(apiKey, "Most Popular");
        }
    }

    class FetchMovieTask extends AsyncTask<String, Void, ArrayList<Movie>> {

        private final String LOG_TAG = FetchMovieTask.class.getSimpleName();

        private final String BASE_URL = "http://api.themoviedb.org/3";
        private final String Q_API_KEY = "api_key";
        private final String P_DISCOVER = "discover";
        private final String P_MOVIE = "movie";
        private final String Q_SORT_BY = "sort_by";
        private String apiKey;

        @Override
        protected ArrayList<Movie> doInBackground(String... params) {
            if (params.length == 0)
                return null;

            this.apiKey = params[0];

            String sort_by;

            if(params[1].equals("Most Popular")){
                sort_by = "popularity.desc";
            }else{
                sort_by = "vote_average.desc";
            }

            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            String jsonMovieList = null;
            try {
                Uri uri = Uri.parse(BASE_URL)
                        .buildUpon()
                        .appendPath(P_DISCOVER)
                        .appendPath(P_MOVIE)
                        .appendQueryParameter(Q_API_KEY, apiKey)
                        .appendQueryParameter(Q_SORT_BY, sort_by)
                        .build();
                URL url = new URL(uri.toString());
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuilder builder = new StringBuilder();

                if (inputStream == null) {
                    return null;
                }

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line).append('\n');
                }

                if (builder.length() == 0) {
                    return null;
                }

                jsonMovieList = builder.toString();

            } catch (IOException e) {
                Log.e(LOG_TAG, "Error ", e);
                return null;
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        Log.e(LOG_TAG, "Error closing steam ", e);
                    }
                }
            }

            try {
                return GetMovieListFromData(jsonMovieList);
            } catch (JSONException e) {
                Log.e(LOG_TAG, "Could not parse JSON ", e);
            }

            return null;
        }

        private ArrayList<Movie> GetMovieListFromData(String jsonString) throws JSONException {
            ArrayList<Movie> movieList = new ArrayList<>();

            JSONObject jsonPageOneResult = new JSONObject(jsonString);
            JSONArray jsonResultsList = jsonPageOneResult.getJSONArray("results");
            for (int i = 0; i < jsonResultsList.length(); i++) {
                JSONObject jsonMovie = jsonResultsList.getJSONObject(i);

                movieList.add(new Movie(jsonMovie.getString("original_title"),
                        jsonMovie.getString("overview"),
                        jsonMovie.getString("poster_path"),
                        jsonMovie.getDouble("vote_average"),
                        jsonMovie.getString("release_date")
                ));
            }
            return movieList;
        }

        @Override
        protected void onPostExecute(ArrayList<Movie> movies) {
            if (movies != null) {
                moviePosterAdapter.clear();
                for (Movie movie : movies) {
                    moviePosterAdapter.add(movie);
                }
            }
        }
    }
}
