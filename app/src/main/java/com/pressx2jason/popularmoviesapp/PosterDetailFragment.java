package com.pressx2jason.popularmoviesapp;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class PosterDetailFragment extends Fragment {

    private Movie movie;
    private final String LOG_TAG = PosterDetailFragment.class.getSimpleName();

    public PosterDetailFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_poster_detail, container, false);
        Intent intent = getActivity().getIntent();
        if (intent != null && intent.hasExtra(Movie.class.getSimpleName())) {
            movie = intent.getExtras().getParcelable(Movie.class.getSimpleName());

            ImageView poster = (ImageView) rootView.findViewById(R.id.imageview_detail_poster);
            Picasso.with(getActivity()).load(movie.getPosterPath()).into(poster);

            getActivity().setTitle(movie.getTitle());

            TextView synopsis = (TextView) rootView.findViewById(R.id.textview_detail_synopsis);
            synopsis.setText(movie.getSynopsis());

            TextView rating = (TextView) rootView.findViewById(R.id.textview_detail_rating);
            rating.setText(movie.getUserRating());

            TextView releaseDate = (TextView) rootView.findViewById(R.id.textview_detail_releaseDate);
            releaseDate.setText(movie.getReleaseDate());
        }
        return rootView;
    }
}
