package com.pressx2jason.popularmoviesapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jason on 10/17/2015.
 */
public class MoviePosterAdapter extends BaseAdapter {

    private Context context;
    private List<Movie> movies = new ArrayList<Movie>();
    private LayoutInflater inflater;
    private final String LOG_TAG = MoviePosterAdapter.class.getSimpleName();

    public MoviePosterAdapter(Context context, List<Movie> movies) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.movies = movies;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        if (convertView == null) {
            view = inflater.inflate(R.layout.fragment_posters_grid, parent, false);
        } else {
            view = convertView;
        }

        if (movies == null || movies.size() == 0) {
            return view;
        }

        Movie movie = getItem(position);

        TextView textView = (TextView) view.findViewById(R.id.textview_posters_grid);
        textView.setText(movie.getTitle());

        ImageView imageView = (ImageView) view.findViewById(R.id.imageview_posters_grid);
        Picasso.with(context).load(movie.getPosterPath()).into(imageView);
        return view;
    }

    @Override
    public int getCount() {
        return movies.size();
    }

    @Override
    public Movie getItem(int position) {
        return movies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void add(Movie movie){
        movies.add(movie);
    }

    public void clear(){
        if(movies != null) {
            movies.clear();
        }
        this.notifyDataSetChanged();
    }
}