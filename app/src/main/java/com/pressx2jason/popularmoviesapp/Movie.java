package com.pressx2jason.popularmoviesapp;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Jason on 10/21/2015.
 */
public class Movie implements Parcelable {
    private String title;
    private String synopsis;
    private String posterPath;
    private double userRating;
    private boolean posterIsNull;
    private String releaseDate;
    private SIZE size;

    private final String LOG_TAG = Movie.class.getSimpleName();

    private final String BASE_URL = "http://image.tmdb.org/t/p/";

    protected Movie(Parcel in) {
        title = in.readString();
        synopsis = in.readString();
        posterPath = in.readString();
        userRating = in.readDouble();
        releaseDate = in.readString();
        posterIsNull = in.readByte() == 1;
        size = SIZE.W500;
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(title);
        dest.writeString(synopsis);
        dest.writeString(posterPath);
        dest.writeDouble(userRating);
        dest.writeString(releaseDate);
        dest.writeString(BASE_URL);
        dest.writeByte((byte) (posterIsNull ? 1 : 0));
    }

    private enum SIZE {
        W29("w92"),
        W154("w154"),
        W185("w185"),
        W342("w342"),
        W500("w500"),
        W780("w780"),
        ORIGINAL("original");

        private String toString;

        SIZE(String s) {
            toString = s;
        }

        @Override
        public String toString() {
            return toString;
        }
    }


    public Movie(String title, String synopsis, String posterPath, double userRating, String releaseDate) {
        this.title = title;
        this.synopsis = synopsis;
        if (posterPath.equals("null")) {
            this.posterPath = "https://assets.tmdb.org/assets/f996aa2014d2ffddfda8463c479898a3/images/no-poster-";
            posterIsNull = true;
        } else {
            this.posterPath = posterPath;
            posterIsNull = false;
        }
        this.userRating = userRating;
        this.releaseDate = releaseDate;
        this.size = SIZE.W185;
    }

    public String getTitle() {
        return title;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public String getPosterPath() {
        if (posterIsNull) {
            return posterPath + getSize() + ".jpg";
        }
        return BASE_URL + '/' + getSize() + '/' + posterPath;
    }

    public String getUserRating() {
        return Double.toString(userRating) + "/ 10";
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getSize() {
        return size.toString();
    }
}
